#pragma once

#include <string>
#include <vector>

/** 
 * @brief Filesystem helpers. 
 * All given paths can be relative or absolute.
 */ 
namespace nada::fs {

    /**
     * Reads a file with a given path and adds each line to a container via 'push_back'.
     * @param path Relative or absolute path to file to read.
     * @param list File will be read line by line into this.
     * @param min_length Lines with characters less than this will be skipped.
     */ 
    void read_lines(const std::string& path, std::vector<std::string>& list, unsigned min_length = 0);

    // Writes given string to specified file. Returns true on write success, otherwise false. File will be created or overwritten.
    bool write_lines(const std::string& path, std::string lines);

    /**
    * Returns all file paths (relative) from this executable's working directory (relative)
    * with a given file extension (case sensitive).
    *
    * @param folder Path to folder. Relative or absolute.
    * @param extension e.g. "png", "jpg", "dat", "json" etc. (without '.' before that). Case sensitive. Leave blank for all files.
    * @note File extension is Case-sensitive.
    * @note Path is given including `folder`. For example: `folder/file.extension`
    * @note '/' characters are used as path separators.
    * @note Only given folder is searched, not subfolders.
    */
    std::vector<std::string> all_files(const std::string& folder, std::string extension = "");

    /**
     * Same as `nada::fs::all_files`, but also searches subfolders and their subfolders of given folder.
     */
    std::vector<std::string> all_files_recursive(const std::string& folder, std::string extension = "");

    /// @brief Returns true, if file at given path exists and is readable.
    bool exists_file(const std::string& path);

    /// @brief Returns true, if given path exists and is a folder.
    bool exists_folder(const std::string& path);

    /// @brief Returns true if the file was successfully deleted.
    bool delete_file(const std::string& filename);

    /** 
     * Creates folder (and in 'path' possibly given subfolders recursively). 
     * @return `true`, if folder was successfully created. False if not or folder already existed.
     */
    bool create_folder(const std::string& path);

    /// @brief Returns true if the folder (and all its contents) was successfully deleted.
    bool delete_folder(const std::string& path);

}
